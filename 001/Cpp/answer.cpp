#include <iostream>
#include <vector>
#include <string>

int main()
{
    try {
        int k = 0, answer = 0;
        std::string current_status, temp;
        std::cout << "Enter k: ";
        std::cin >> k;
        if (std::cin.fail())
            throw "Char entered when except integer";
        std::cout << "Enter current status: ";
        std::cin >> current_status;
        for (auto& i : current_status)
        {
            if ( i < 48 || i > 57)
                throw "Char entered when except number";
        }
        if (current_status.length() != k)
            throw "current_status.length != k";
        for (int i = 0;i < k; i++)
        {
            std::cout << "Enter router number " << i+1 << ": ";
            std::cin >> temp;
            for (auto& i : temp)
            {
                if ( i < 48 || i > 57)
                    throw "Char entered when except number";
            }
            if (temp.find_first_of(current_status[i]) <= (temp.length() - temp.find_last_of(current_status[i]))) {
                answer += temp.find_first_of(current_status[i]);
            } else {
                answer += (temp.length() - temp.find_last_of(current_status[i]));
            }
        }
        std::cout << "Answer: " << answer << std::endl;
    } catch(const char* e) {
        std::cout << "main() failed with: " << e << std::endl;
    }
    return 0;
}

